package PingPong;

import fi.jumi.actors.ActorRef;
import fi.jumi.actors.ActorThread;
import fi.jumi.actors.Actors;
import fi.jumi.actors.MultiThreadedActors;
import fi.jumi.actors.eventizers.dynamic.DynamicEventizerProvider;
import fi.jumi.actors.listeners.CrashEarlyFailureHandler;
import fi.jumi.actors.listeners.NullMessageListener;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by JanDennis on 12.06.2016.
 */
public class Main {

  public static void main(String[] args) {
    ExecutorService actorsThreadPool = Executors.newCachedThreadPool();
    Actors actors = new MultiThreadedActors(
            actorsThreadPool,
            new DynamicEventizerProvider(),
            new CrashEarlyFailureHandler(),
            new NullMessageListener()
    );

    ActorThread pingThread = actors.startActorThread();
    ActorThread pongThread = actors.startActorThread();


    PingPongActor ping = new PingPongActor("Ping!");
    PingPongActor pong = new PingPongActor("     Pong!");

    ActorRef<PingPongBehavior> mailboxPing = pingThread.bindActor(PingPongBehavior.class, ping);
    ActorRef<PingPongBehavior> mailboxPong = pongThread.bindActor(PingPongBehavior.class, pong);

    ping.setMailbox(mailboxPing);
    pong.setMailbox(mailboxPong);

    mailboxPing.tell().ping(10, mailboxPong);


    pingThread.stop();
    pongThread.stop();

    actorsThreadPool.shutdown();


  }
}