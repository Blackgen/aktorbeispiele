package PingPong;

import fi.jumi.actors.ActorRef;

/**
 * Created by JanDennis on 12.06.2016.
 */
public interface PingPongBehavior {
  void ping(int count, ActorRef<PingPongBehavior> caller);
}
