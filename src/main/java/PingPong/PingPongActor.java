package PingPong;

import fi.jumi.actors.ActorRef;

/**
 * Created by JanDennis on 12.06.2016.
 */
public class PingPongActor implements PingPongBehavior {
  private String name;
  private ActorRef<PingPongBehavior> mailbox;

  public PingPongActor(String name) {
    this.name = name;
  }

  public void ping(int count, ActorRef<PingPongBehavior> caller) {
    System.out.println(name);
    if (count > 0) {
      caller.tell().ping(count - 1, mailbox);
    }
  }

  public void setMailbox(ActorRef<PingPongBehavior> mailbox) {
    this.mailbox = mailbox;
  }
}
