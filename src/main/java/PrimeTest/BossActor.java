package PrimeTest;

import fi.jumi.actors.ActorRef;
import fi.jumi.actors.ActorThread;
import fi.jumi.actors.Actors;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JanDennis on 12.06.2016.
 */
public class BossActor implements BossBehavior {
  ActorRef<BossBehavior> mailbox;
  List<ActorRef<WorkerBehavior>> worker = new ArrayList<>();
  List<ActorThread> workerThreads = new ArrayList<>();
  Actors system;

  public BossActor(Actors system) {
    this.system = system;
  }

  @Override
  public void searchForPrimes(int min, int max) {
    int w = 0;
    for (int i = min; i < max; i++) {
      worker.get(w).tell().TestNumber(i);
      w = (w == worker.size() - 1) ? 0 : w + 1;
    }
  }

  @Override
  public void foundPrime(int i) {
    System.out.println("Found  " + i);
  }

  @Override
  public void spawnWorkers(int count) {
    for (int i = 0; i < count; i++) {
      ActorThread workerThread =system.startActorThread();
      workerThreads.add(workerThread);
      worker.add(workerThread.bindActor(WorkerBehavior.class, new WorkerActor(mailbox)));
    }
  }

  public void killWorkers() {
    workerThreads.stream().forEach(w -> w.stop());
  }

  public void setMailbox(ActorRef<BossBehavior> mailbox) {
    this.mailbox = mailbox;
  }
}
