package PrimeTest;

import fi.jumi.actors.ActorRef;

/**
 * Created by JanDennis on 12.06.2016.
 */
public class WorkerActor implements WorkerBehavior {
  private ActorRef<BossBehavior> bossAdress;

  public WorkerActor(ActorRef<BossBehavior> bossAdress) {
    this.bossAdress = bossAdress;
  }

  @Override
  public void TestNumber(int i) {
    if (isPrime(i)) bossAdress.tell().foundPrime(i);
  }

  /*
  Test if x is a prime number.
  Source: http://rosettacode.org/wiki/Sequence_of_primes_by_Trial_Division#Java
   */
  public static boolean isPrime(long x) {
    if (x < 3 || x % 2 == 0)
      return x == 2;

    long max = (long) Math.sqrt(x);
    for (long n = 3; n <= max; n += 2) {
      if (x % n == 0) {
        return false;
      }
    }
    return true;
  }
}
