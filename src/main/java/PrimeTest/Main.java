package PrimeTest;

import fi.jumi.actors.ActorRef;
import fi.jumi.actors.ActorThread;
import fi.jumi.actors.Actors;
import fi.jumi.actors.MultiThreadedActors;
import fi.jumi.actors.eventizers.dynamic.DynamicEventizerProvider;
import fi.jumi.actors.listeners.CrashEarlyFailureHandler;
import fi.jumi.actors.listeners.NullMessageListener;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by JanDennis on 12.06.2016.
 */
public class Main {
  public static void main(String[] args) {
    ExecutorService actorsThreadPool = Executors.newCachedThreadPool();
    Actors actors = new MultiThreadedActors(
            actorsThreadPool,
            new DynamicEventizerProvider(),
            new CrashEarlyFailureHandler(),
            new NullMessageListener()
    );

    ActorThread bossThread = actors.startActorThread();
    BossActor boss = new BossActor(actors);
    ActorRef<BossBehavior> bossAdress = bossThread.bindActor(BossBehavior.class, boss);
    boss.setMailbox(bossAdress);

    bossAdress.tell().spawnWorkers(12);
    bossAdress.tell().searchForPrimes(100, 500);


    try {
      System.in.read();
    } catch (IOException e) {
      e.printStackTrace();
    }

    bossAdress.tell().killWorkers();


    bossThread.stop();
    actorsThreadPool.shutdown();
    System.out.println("fin");
  }
}
