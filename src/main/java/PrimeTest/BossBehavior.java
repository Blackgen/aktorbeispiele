package PrimeTest;

/**
 * Created by JanDennis on 12.06.2016.
 */
public interface BossBehavior {
  void searchForPrimes(int min, int max);

  void foundPrime(int i);

  void spawnWorkers(int count);

  void killWorkers();
}
